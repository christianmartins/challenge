package com.campsite.controllers;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


@ContextConfiguration
@Test(groups="my-integration-tests")
public class ReservationControllerTest extends AbstractTestNGSpringContextTests {

	@BeforeClass
	public void setEnviroment() {

		RestAssured.baseURI = "http://localhost:8080";
		
		// Request
		RequestSpecification request = RestAssured.given();
		
		// Add a header stating the Request body is a JSON
		request.header("Content-Type", "application/json");
		
		// Reservation for tomorrow and the day after tomorrow
		Date arrivalDate = Date.from(Instant.now().plus(2, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS));
		Date departureDate = Date.from(Instant.now().plus(3, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS));
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		// Check if an overlapping reservation exists
		Response response = request.get("/reservations/?arrivalDate=" + sdf.format(arrivalDate) + "&departureDate=" + sdf.format(departureDate));
		
		if (response.statusCode() == 200) {

			List<Long> ids = null;

			try {
				ids = response.jsonPath().getList("_embedded.reservationList.id");
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			// If an overlapping reservation exists, delete it. This is just for testing enviroment
			if (ids != null && ids.size() > 0) {
				request.delete("/reservations/" + ids.get(0));
			}
		}
		
	}

	// Make 100 parallel calls to save with the same overlapping dates
	@SuppressWarnings("unchecked")
	@Test(threadPoolSize = 100, invocationCount = 100)
	public void testConcurrency() {

		RequestSpecification request = RestAssured.given();

		// Add a header stating the Request body is a JSON
		request.header("Content-Type", "application/json");
		
		// Reservation for tomorrow and the day after tomorrow
		Date arrivalDate = Date.from(Instant.now().plus(2, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS));
		Date departureDate = Date.from(Instant.now().plus(3, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS));

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		org.json.simple.JSONObject requestParams = new org.json.simple.JSONObject();
		requestParams.put("fullName", "John Doe");
		requestParams.put("email", "test@gmail.com");
		requestParams.put("arrivalDate",  sdf.format(arrivalDate));
		requestParams.put("departureDate",  sdf.format(departureDate));

		request.body(requestParams.toJSONString());

		Response response = request.post("/reservations");
		
		// Check if 200 (ok) or 400 (bad request due to overlapping records)
		Assert.assertTrue(response.statusCode()==400 || response.statusCode()==200);
		
	}
	
	@AfterClass
	public void checkUniqueReservation() {
		
		RequestSpecification request = RestAssured.given();
		
		// Add a header stating the Request body is a JSON
		request.header("Content-Type", "application/json");
		
		// Reservation for tomorrow and the day after tomorrow
		Date arrivalDate = Date.from(Instant.now().plus(2, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS));
		Date departureDate = Date.from(Instant.now().plus(3, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS));
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		// Search for our created record
		Response response = request.get("/reservations/?arrivalDate=" + sdf.format(arrivalDate) + "&departureDate=" + sdf.format(departureDate));
		
		// Check that only one record has been created
		if(response.statusCode()==200) {
			Assert.assertTrue(response.jsonPath().getList("_embedded.reservationList.id").size()==1);
		}else {
			Assert.fail();
		}
		
		
	}

}
