package com.campsite.validators;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import com.campsite.domains.Reservation;
import com.campsite.exceptions.ReservationConstraintException;

public class ReservationValidator {

	public static void validateReservation(Reservation reservation) {

		// Arrival date must not be empty
		if (reservation.getArrivalDate() == null)
			throw new ReservationConstraintException("Arrival date must not be empty");

		// Departure date must not be empty
		if (reservation.getDepartureDate() == null)
			throw new ReservationConstraintException("Departure date must not be empty");

		// Departure date must be greater than arrival date
		if (reservation.getArrivalDate().compareTo(reservation.getDepartureDate()) >= 0)
			throw new ReservationConstraintException("Departure date must be greater than arrival date");

		// Campsite can be reserved for max 3 days
		long diff = reservation.getDepartureDate().getTime() - reservation.getArrivalDate().getTime();
		if (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) > 3)
			throw new ReservationConstraintException("Campsite cannot be reserved for more than 3 days");

		// Campsite can be reserved up to 1 month in advance
		diff = reservation.getArrivalDate().getTime() - Calendar.getInstance().getTime().getTime();
		if (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) > 30)
			throw new ReservationConstraintException("Campsite can be reserved up to 1 month in advance");

	}

}
