package com.campsite.controllers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.campsite.domains.Reservation;
import com.campsite.services.ReservationService;

@RestController
public class ReservationController {

	@Autowired
	private ReservationService reservationService;

	@GetMapping(value="/reservations", produces="application/json; charset=UTF-8")
	Resources<Resource<Reservation>> listAll(@RequestParam(required=false, name="arrivalDate") @DateTimeFormat(pattern="yyyy-MM-dd") Date arrivalDate, @RequestParam(required=false, name="departureDate") @DateTimeFormat(pattern="yyyy-MM-dd") Date departureDate) {
		
		// If date not specified, use today and today + 30 days
		if(arrivalDate==null) arrivalDate = Date.from(Instant.now().truncatedTo(ChronoUnit.DAYS));
		if(departureDate==null) {
			departureDate = Date.from(Instant.now().plus(30, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS));
		}
		
		List<Resource<Reservation>> reservations = reservationService.findByDate(arrivalDate, departureDate).stream()
			.map(reservation -> new Resource<>(reservation,
				linkTo(methodOn(ReservationController.class).getById(reservation.getId())).withSelfRel(),
				linkTo(methodOn(ReservationController.class).listAll(null, null)).withRel("reservations")))
			.collect(Collectors.toList());
		
		return new Resources<>(reservations,
			linkTo(methodOn(ReservationController.class).listAll(null, null)).withSelfRel());
	}

	@PostMapping("/reservations")
	Reservation createReservation(@RequestBody Reservation newReservation) {
		return reservationService.saveWithoutOverlapping(newReservation);
	}


	@GetMapping(value="/reservations/{id}", produces="application/json; charset=UTF-8")
	Resource<Reservation> getById(@PathVariable Long id) {
		
		Reservation reservation = reservationService.findById(id);
		
		return new Resource<>(reservation,
			linkTo(methodOn(ReservationController.class).getById(id)).withSelfRel(),
			linkTo(methodOn(ReservationController.class).listAll(null, null)).withRel("reservations"));
	}

	@PutMapping("/reservations/{id}")
	Reservation updateReservation(@RequestBody Reservation newReservation, @PathVariable Long id) {
		
		Reservation reservation = reservationService.findById(id);
		reservation.setFullName(newReservation.getFullName());
		reservation.setEmail(newReservation.getEmail());
		reservation.setArrivalDate(newReservation.getArrivalDate());
		reservation.setDepartureDate(newReservation.getDepartureDate());;
		
		return reservationService.saveWithoutOverlapping(reservation);
		
	}

	@DeleteMapping("/reservations/{id}")
	void deleteReservation(@PathVariable Long id) {
		reservationService.deleteById(id);
	}
}
