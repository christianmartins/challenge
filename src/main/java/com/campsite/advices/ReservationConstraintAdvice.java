package com.campsite.advices;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.campsite.exceptions.ReservationConstraintException;

@EnableWebMvc
@ControllerAdvice
public class ReservationConstraintAdvice {

	@ResponseBody
	@ExceptionHandler(ReservationConstraintException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	String reservationConstraintHandler(ReservationConstraintException ex) {
		return ex.getMessage();
	}
}
