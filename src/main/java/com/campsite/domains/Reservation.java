package com.campsite.domains;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
public class Reservation {

	public Reservation() {
	}

	private @Id @GeneratedValue Long id;
	
	@NotNull
	@NotBlank
	private String fullName;
	
	@Email
	@NotBlank
	private String email;
	
	@NotNull
	@Future
	@Temporal(TemporalType.DATE)
	private Date arrivalDate;
	
	@NotNull
	@Future
	@Temporal(TemporalType.DATE)
	private Date departureDate;
	
	public Reservation(Long id, String fullName, String email, Date arrivalDate, Date departureDate) {
		super();
		this.id = id;
		this.fullName = fullName;
		this.email = email;
		this.arrivalDate = arrivalDate;
		this.departureDate = departureDate;
	}

	public Long getId() {
		return id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	
}
