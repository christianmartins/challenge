package com.campsite.exceptions;

@SuppressWarnings("serial")
public class ReservationConstraintException extends RuntimeException {

	public ReservationConstraintException(String message) {
		super("Constraint violation: " + message);
	}
}
