package com.campsite.exceptions;

@SuppressWarnings("serial")
public class ReservationOverlappingException extends RuntimeException {

	public ReservationOverlappingException() {
		super("Another reservation exists for the specified date range");
	}
}
