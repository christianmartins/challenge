package com.campsite.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.campsite.domains.Reservation;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {
	
	// Query to find overlapping of campsite reservations
	@Query("SELECT t FROM Reservation t WHERE ?2 >= t.arrivalDate AND t.departureDate > ?1")
    List <Reservation> findByDate(Date arrivalDate, Date departureDate);
	
}
