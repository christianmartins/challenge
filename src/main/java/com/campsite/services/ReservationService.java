package com.campsite.services;

import java.util.Date;
import java.util.List;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.campsite.domains.Reservation;
import com.campsite.exceptions.ReservationConstraintException;
import com.campsite.exceptions.ReservationNotFoundException;
import com.campsite.exceptions.ReservationOverlappingException;
import com.campsite.repositories.ReservationRepository;
import com.campsite.validators.ReservationValidator;

@Component
public class ReservationService {

	private final ReservationRepository repository;

	public ReservationService(ReservationRepository repository) {
		this.repository = repository;
	}

	// Find reservations by date
	public List<Reservation> findByDate(Date dateFrom, Date dateTo) {
		return repository.findByDate(dateFrom, dateTo);
	}

	// Save reservations without overlapping
	public synchronized Reservation saveWithoutOverlapping(Reservation reservation) {
		
		ReservationValidator.validateReservation(reservation);
		try {
			// Check if overlapping exists
			List<Reservation> overlappingList = repository.findByDate(reservation.getArrivalDate(),
					reservation.getDepartureDate());
	
			if (!overlappingList.isEmpty() && (overlappingList.get(0).getId()!=reservation.getId())) {
				// If exists throw exception
				throw new ReservationOverlappingException();
			} else {
				// If not write and flush
				reservation = repository.saveAndFlush(reservation);
			}
	
			/**
			 * After flushing check again, if another one exists, delete my reservation
			 * This is useful for multiple instance deployments. "Synchronized" solves mono
			 * instance concurrency problems but the following code guarantees the same
			 * result with a multiple instance application. Removing "synchronized" from the
			 * method signature simulates a multiple instances deployment and requires some deletes 
			 * when executing TestNG tests
			 * 
			 * Another approach would be to delegate this to the database with a conditional
			 * insert (if the database provides this functionality) or inserting using a
			 * stored procedure
			 * 
			 * Another approach would be to mark the method with @Transactional(rollbackFor = ReservationOverlappingException.class, isolation = Isolation.SERIALIZABLE)
			 * This would be a performance penalty as it would execute operations in a serialized way 
			 * and it would also require to do some code changes as JPA doesn't support it out of the box
			 * 
			 */
			overlappingList = repository.findByDate(reservation.getArrivalDate(),
					reservation.getDepartureDate());
			if (overlappingList.size() == 1) {
				return reservation;
			} else {
				repository.delete(reservation);
				System.out.println("Concurrency delete");
				throw new ReservationOverlappingException();
			}
		}catch(ConstraintViolationException cve) {
			@SuppressWarnings("rawtypes")
			ConstraintViolation cv = cve.getConstraintViolations().iterator().next();
			throw new ReservationConstraintException(cv.getPropertyPath().toString().concat(" ") + cv.getMessage());
		}

	}

	// Get reservations by id
	public Reservation findById(Long id) {
		return repository.findById(id).orElseThrow(() -> new ReservationNotFoundException(id));
	}

	// Delete reservations by id
	public void deleteById(Long id) {
		try {
			repository.deleteById(id);
		}catch (EmptyResultDataAccessException e) {
			throw new ReservationNotFoundException(id);
		}
	}

}
